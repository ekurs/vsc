// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

let myStatusBarItem: vscode.StatusBarItem;

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	const testCommand = 'ekurs.startTest';

	vscode.workspace.openTextDocument(`${vscode.env.appRoot}/package.json`).then((document) => {
		const text = JSON.parse(document.getText());
		if(!text.name.includes('ekurs-')) {
			vscode.commands.executeCommand('workbench.action.terminal.toggleTerminal');

			const terminal = vscode.window.createTerminal(`eKurs.Server`);
			terminal.sendText("npm install");
			terminal.sendText("npm run server");
			terminal.show();
			
			myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Right, 100);
			myStatusBarItem.command = testCommand;
			myStatusBarItem.text = 'eKurs - Testuj';
			myStatusBarItem.show();

			context.subscriptions.push(myStatusBarItem);
		}
	});	

	let disposable = vscode.commands.registerCommand(testCommand, () => {
		const terminal = vscode.window.createTerminal(`eKurs.Test`);
		terminal.sendText("npm run test");
		terminal.show();
	});

	context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
